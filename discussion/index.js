// alert('Hello World!')

// Adding clear/reset option after creating/editing posts
let createTitleInput = document.querySelector("#txt-title");
let createBodyInput = document.querySelector("#txt-body");
let editIdInput = document.querySelector("#txt-edit-id");
let editTitleInput = document.querySelector("#txt-edit-title");
let editBodyInput = document.querySelector("#txt-edit-body");


// Mock Database
let posts = [];
// Posts ID
let count = 1;

// Add post - eventListener
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    e.preventDefault()
    posts.push({
        id: count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    })
    // count will increment everytime a new posts is added.
    count++
    console.log(posts)
    alert("Post successfully added!")
    showPosts()
    resetAfterPost()
});

const showPosts = () => {
    // create a variable that will contain all the posts.
    let postEntries = "";

    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        
        `
    })

    console.log(postEntries)

    document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// Edit Post Button
// We will create a function that will be called in the onClick() event and will pass the value in the Update Form input box. 

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    // Pass the id, title, and body from the post to be updated in the Edit Post Form
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
};

// Update post - eventListener
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();

    for (let i = 0; i < posts.length; i++) {
        if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;

            showPosts(posts)
            alert("Succesfully Updated Post!");
            resetAfterEdit();
            break;
        }
    }
});

// Activity
// Delete post
const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString() !== id) {
            return post
        }
    })
    document.querySelector(`#post-${id}`).remove()
    alert('Post successfully deleted!')
    console.log(posts)
};

// applying a stretch goal

// this will automatically reset the input fields of Add Post after creating a post
let resetAfterPost = () => {
    createTitleInput.value = '';
    createBodyInput.value = '';
};
// this will automatically reset the input fields of Edit Post after editing a post
let resetAfterEdit = () => {
    editIdInput.value = '';
    editTitleInput.value = '';
    editBodyInput.value = '';
};
